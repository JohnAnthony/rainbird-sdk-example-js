const {
	start,
	query,
	response,
	RESPONSE_TYPE_QUESTION,
	URL_COMMUNITY,
} = require("@rainbird/sdk");
const prompts = require("prompts");

const APIKey = "37582f71-b9b1-4451-b9e6-c130c7fd2412";
const KnowledgeMapID = "2fd1be28-b38d-4fa3-8b9d-b0976821912c";

const goal = {
	subject: "John",
	relationship: "speaks",
	object: "",
};

async function main() {
	const { sessionID } = await start(URL_COMMUNITY, APIKey, KnowledgeMapID);
	let { type, data } = await query(
		URL_COMMUNITY,
		sessionID,
		goal.subject,
		goal.relationship,
		goal.object
	);

	while (type === RESPONSE_TYPE_QUESTION) {
		const { question } = data;
		const prompted = await prompts({
			type: "text",
			name: "userInput",
			message: question.prompt,
			validate: (s) => s.length > 0,
		});

		const next = await response(URL_COMMUNITY, sessionID, [
			{
				subject: question.subject || prompted.userInput,
				relationship: question.relationship,
				object: question.object || prompted.userInput,
				certainty: 100,
			},
		]);
		type = next.type;
		data = next.data;
	}

	for (const result of data) {
		const { subject, relationship, object, certainty } = result;
		console.log(`${subject} - ${relationship} - ${object} (${certainty}%)`);
	}
}

main();
